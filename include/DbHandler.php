<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Gurunath kulkarni
 */
class DbHandler
{

    private $conn;

    public function __construct()
    {
        require_once dirname(__FILE__) . '/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    // common functions ////

    public function getCurrentDate()
    {
        return date('Y-m-d H:i:s');
    }


    public function getYear()
    {
        return date('Y');
    }

    //// end of common function ////

    // function of Rest API of MY Project
    /////////////////////////////////////////  ORGANIZATION ////////////////////////////////////////////////////////////
    public function createOrganization($organization_name, $organzation_address, $organization_date, $phone_number)
    {
        $response = array();
        $query_set = "INSERT INTO `organization`(`organization_name`, `organzation_address`, `organization_date`, `phone_number`) VALUES ('$organization_name','$organzation_address','$organization_date','$phone_number')";
        $query = mysqli_query($this->conn, $query_set);
        if ($query) {
            // $last_id = $this->conn->insert_id;
            return 1;
        } else {
            return 0;
        }
    }

    public function EditOrganization($organization_id, $organization_name, $organzation_address, $organization_date, $phone_number)
    {
        $response = array();
        $query_set = "UPDATE `organization` SET `organization_name`='$organization_name',`organzation_address`='$organzation_address',`organization_date`='$organization_date',`phone_number`='$phone_number' WHERE organization_id='$organization_id'";
        $query = mysqli_query($this->conn, $query_set);
        if ($query) {
            // $last_id = $this->conn->insert_id;
            // return $last_id;
            return 1;
        } else {
            return 0;
        }
    }

    public function getOrganization()
    {
        $q = "SELECT * FROM `organization` WHERE 1";
        $query = mysqli_query($this->conn, $q);
        $a = array();
        $res = array();
        if ($query->num_rows > 0) {
            // output data of each row
            while ($row = $query->fetch_assoc()) {
                $res['organization_id'] = $row["organization_id"];
                $res['organization_name'] = $row["organization_name"];
                $res['organzation_address'] = $row['organzation_address'];
                $res['organization_date'] = $row['organization_date'];
                $res['phone_number'] = $row['phone_number'];
                array_push($a, $res);
            }
        }

        echo json_encode($a);
    }

    ////////////////////////////// END ORGAIZATION ////////////////////////////////////////////////////////////////////

    // //////////////////////////START OF DIRECTOR ///////////////////////////////////////////////////////////////////

    public function createDirector($director_name, $director_address, $phonenumber)
    {
        $response = array();
        $query_set = "INSERT INTO `director`( `director_name`, `director_address`, `phonenumber`) VALUES ('$director_name','$director_address','$phonenumber')";
        $query = mysqli_query($this->conn, $query_set);
        if ($query) {
            // $last_id = $this->conn->insert_id;
            return 1;
        } else {
            return 0;
        }
    }

    public function EditDirector($director_id, $director_name, $director_address, $phonenumber)
    {
        $response = array();
        $query_set = "UPDATE `director` SET `director_name`='$director_name',`director_address`='$director_address',`phonenumber`='$phonenumber' WHERE director_id='$director_id'";
        $query = mysqli_query($this->conn, $query_set);
        if ($query) {
            // $last_id = $this->conn->insert_id;
            return 1;
        } else {
            return 0;
        }
    }

    public function getDirector()
    {
        $q = "SELECT * FROM `director` WHERE 1";
        $query = mysqli_query($this->conn, $q);
        $a = array();
        $res = array();
        if ($query->num_rows > 0) {
            // output data of each row
            while ($row = $query->fetch_assoc()) {
                $res['director_id'] = $row["director_id"];
                $res['director_name'] = $row["director_name"];
                $res['director_address'] = $row['director_address'];
                $res['phonenumber'] = $row['phonenumber'];
                array_push($a, $res);
            }
        }

        echo json_encode($a);
    }

    public function directorDelete($director_id)
    {
        $response = array();
        $query_set = "DELETE FROM `director` WHERE `director_id`='$director_id'";
        $query = mysqli_query($this->conn, $query_set);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }

    ///////////////////////////END DIRECTOR ///////////////////////////////////////////////////////////////////////////

    ///////////////////////// START OF GROUP ///////////////////////////////////////////////////////
    public function createGroup($group_name, $member_count, $no_of_months, $amount_value, $installement_amount, $start_date, $end_date)
    {
        $response = array();
        $modified_date = $this->getCurrentDate();
        $query_set = "INSERT INTO `groupMaster`(`group_name`, `member_count`, `no_of_months`, `amount_value`, `installement_amount`,`start_month`, `end_month`, `modified_date`) VALUES ('$group_name','$member_count','$no_of_months','$amount_value','$installement_amount','$start_date','$end_date','$modified_date')";
        $query = mysqli_query($this->conn, $query_set);
        if ($query) {
            $last_id = $this->conn->insert_id;
            $obj = (object) [
                'status' => "successful",
                'status_code' => '1',
                'group_id' => $last_id,
            ];
            echo json_encode($obj);
        } else {
            $obj = (object) [
                'status' => "failed",
                'status' => '0',
            ];
            echo json_encode($obj);
        }
    }

    public function EditGroup($group_id, $group_name, $member_count, $no_of_months, $amount_value, $installement_amount, $start_date, $end_date)
    {
        $response = array();
        $query_set = "UPDATE `groupMaster` SET `group_name`='$group_name',`member_count`='$member_count',`no_of_months`='$no_of_months',`amount_value`='$amount_value',`installement_amount`='$installement_amount',`start_month` ='$start_date',`end_month`='$end_date','modified_date'='$modified_date' WHERE `group_id`= '$group_id'";
        $query = mysqli_query($this->conn, $query_set);
        if ($query) {
            $obj = (object) [
                'status' => "successful",
                'status_code' => '1',
            ];
            echo json_encode($obj);
        } else {
            $obj = (object) [
                'status' => "failed",
                'status' => '0',
            ];
            echo json_encode($obj);
        }
    }

    public function getGroup()
    {
        $q = "SELECT * FROM `groupMaster` WHERE 1";
        $query = mysqli_query($this->conn, $q);
        $a = array();
        $res = array();
        if ($query->num_rows > 0) {
            // output data of each row
            while ($row = $query->fetch_assoc()) {
                $res['group_id'] = $row["group_id"];
                $res['group_name'] = $row["group_name"];
                $res['member_count'] = $row['member_count'];
                $res['no_of_months'] = $row['no_of_months'];
                $res['amount_value'] = $row['amount_value'];
                $res['installement_amount'] = $row['installement_amount'];
                array_push($a, $res);
            }
        }

        echo json_encode($a);
    }

    public function createGroupChart($group_id, $fixbidding_per, $dividend, $sub, $prize_amt)
    {
        $response = array();
        $modified_date = $this->getCurrentDate();
        $query_set = "INSERT INTO `groupChart`(`group_id`, `fixbidding_per`, `dividend`, `sub`, `prize_amt`,`modified_date`) VALUES ('$group_id','$fixbidding_per','$dividend','$sub','$prize_amt','$modified_date')";
        $query = mysqli_query($this->conn, $query_set);
        if ($query) {
            return 0;
        } else {
            return 1;
        }
    }
///////////////////////////////// END OF GROUP /////////////////////////////////////////////////////////

/////////////////////////////////last bracket////////////////////////////////////////////////////////////

////////////////////////////USER DATA API //////////////////////////////
    public function createUser($user_fname, $user_lname, $phonenumber, $user_address, $username, $password, $join_date, $dob, $role, $modified_date)
    {
        $response = array();
        $query_set = "INSERT INTO `User`(`firstname`, `lastname`,`phonenumber`, `address`, `username`, `password`, `joindate`, `dob`, `role`, `modifieddate`) VALUES ('$user_fname','$user_lname','$phonenumber','$user_address','$username','$password','$join_date','$dob','$role','$modified_date')";
        $query = mysqli_query($this->conn, $query_set);
        if ($query) {
            return 0;
        } else {
            return 1;
        }
    }

    public function getUser()
    {
        $q = "SELECT * FROM `User` WHERE 1";
        $query = mysqli_query($this->conn, $q);
        $a = array();
        $res = array();
        if ($query->num_rows > 0) {
            // output data of each row
            while ($row = $query->fetch_assoc()) {
                $res['user_id'] = $row["user_id"];
                $res['firstname'] = $row["firstname"];
                $res['lastname'] = $row['lastname'];
                $res['phonenumber'] = $row['phonenumber'];
                $res['address'] = $row['address'];
                $res['username'] = $row['username'];
                $res['password'] = $row['password'];
                $res['joindate'] = $row['joindate'];
                $res['dob'] = $row['dob'];
                $res['role'] = $row['role'];
                $res['modified_date'] = $row['modifieddate'];
                array_push($a, $res);
            }
        }
        echo json_encode($a);
    }
    public function deleteUser($user_id)
    {
        $response = array();
        $query_set = "DELETE FROM `User` WHERE `user_id`='$user_id'";
        $query = mysqli_query($this->conn, $query_set);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }

    public function EditUser($user_id, $user_fname, $user_lname, $phonenumber, $user_address, $username, $password, $join_date, $dob, $role, $modified_date)
    {
        $response = array();
        $query_set = "UPDATE `User` SET `firstname`='$user_fname',`lastname`='$user_lname',`phonenumber`='$phonenumber',`address`='$user_address',`username`='$username',`password`='$password',`role`='$role',`modifieddate`='$modified_date' WHERE `user_id`='$user_id'";
        $query = mysqli_query($this->conn, $query_set);
        if ($query) {
            $obj = (object) [
                'status' => "successful",
                'status_code' => '1',
            ];
            echo json_encode($obj);
        } else {
            $obj = (object) [
                'status' => "failed",
                'status' => '0',
            ];
            echo json_encode($obj);
        }
    }



    public function getUserAsOption($option)
    {
        $q = "SELECT * FROM `User` WHERE `role`='$option' ";
        $query = mysqli_query($this->conn, $q);
        $a = array();
        $res = array();
        if ($query->num_rows > 0) {
            // output data of each row
            while ($row = $query->fetch_assoc()) {
                $res['user_id'] = $row["user_id"];
                $res['firstname'] = $row["firstname"];
                $res['lastname'] = $row['lastname'];
                $res['phonenumber'] = $row['phonenumber'];
                $res['address'] = $row['address'];
                $res['username'] = $row['username'];
                $res['password'] = $row['password'];
                $res['joindate'] = $row['joindate'];
                $res['dob'] = $row['dob'];
                $res['role'] = $row['role'];
                $res['modified_date'] = $row['modifieddate'];
                array_push($a, $res);
            }
        }
        echo json_encode($a);
    }
/////////////////////////////////////USER API END /////////////////////////////////

    public function createBank($bank_name, $account_number, $ifsccode, $branchname, $modified_date)
    {
        $query_set = "INSERT INTO `bankMaster`(`bank_name`, `account_number`, `ifsccode`, `branchname`, `modifieddate`) VALUES ('$bank_name','$account_number','$ifsccode','$branchname','$modified_date')";
        $query = mysqli_query($this->conn, $query_set);
        if ($query) {
            return 0;
        } else {
            return 1;
        }
    }

    public function getBank()
    {
        $q = "SELECT * FROM `bankMaster` WHERE 1";
        $query = mysqli_query($this->conn, $q);
        $a = array();
        $res = array();
        if ($query->num_rows > 0) {
            // output data of each row
            while ($row = $query->fetch_assoc()) {
                $res['bank_id'] = $row["bank_id"];
                $res['bank_name'] = $row['bank_name'];
                $res['account_number'] = $row["account_number"];
                $res['ifsccode'] = $row['ifsccode'];
                $res['branchname'] = $row['branchname'];
                $res['modifieddate'] = $row['modifieddate'];
                array_push($a, $res);
            }
        }
        echo json_encode($a);
    }

    public function EditBank($bank_id, $bank_name, $account_number, $ifsccode, $branchname, $modified_date)
    {
        $response = array();
        $query_set = "UPDATE `bankMaster` SET `bank_name`='$bank_name' ,`account_number`='$account_number',`ifsccode`='$ifsccode',`branchname`='$branchname',`modifieddate`='$modified_date' WHERE `bank_id` ='$bank_id'";
        $query = mysqli_query($this->conn, $query_set);
        if ($query) {
            $obj = (object) [
                'status' => "successful",
                'status_code' => '1',
            ];
            echo json_encode($obj);
        } else {
            $obj = (object) [
                'status' => "failed",
                'status' => '0',
            ];
            echo json_encode($obj);
        }
    }

    public function deleteBank($bank_id)
    {
        $response = array();
        $query_set = "DELETE FROM `bankMaster` WHERE `bank_id`='$bank_id'";
        $query = mysqli_query($this->conn, $query_set);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }

    ////////////////end of bank ///////////////////////

    ///////////////start of member /////////////////////
    public function createMember($member_name, $member_address, $phonenumber, $member_lastname, $adhar_number, $city, $state, $date_of_birth, $modified_date)
    {
        $query_set = "INSERT INTO `bankMaster`(`account_number`, `ifsccode`, `branchname`, `modifieddate`) VALUES ('$account_number','$ifsccode','$branchname','$modfied_date')";
        $query = mysqli_query($this->conn, $query_set);
        if ($query) {
            return 0;
        } else {
            return 1;
        }
    }

    public function EditMember($member_id, $member_name, $member_address, $phonenumber, $member_lastname, $adhar_number, $city, $state, $date_of_birth, $modified_date)
    {
        $response = array();
        $query_set = "UPDATE `bankMaster` SET `account_number`='$account_number',`ifsccode`='$ifsccode',`branchname`='$branchname',`modifieddate`='$modfied_date' WHERE `bank_id` ='$bank_id'";
        $query = mysqli_query($this->conn, $query_set);
        if ($query) {
            $obj = (object) [
                'status' => "successful",
                'status_code' => '1',
            ];
            echo json_encode($obj);
        } else {
            $obj = (object) [
                'status' => "failed",
                'status' => '0',
            ];
            echo json_encode($obj);
        }
    }

    public function deleteMember($member_id)
    {
        $response = array();
        $query_set = "DELETE FROM `bankMaster` WHERE `bank_id`='$bank_id'";
        $query = mysqli_query($this->conn, $query_set);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getMember()
    {
        $q = "SELECT * FROM `bankMaster` WHERE 1";
        $query = mysqli_query($this->conn, $q);
        $a = array();
        $res = array();
        if ($query->num_rows > 0) {
            // output data of each row
            while ($row = $query->fetch_assoc()) {
                $res['bank_id'] = $row["bank_id"];
                $res['account_number'] = $row["account_number"];
                $res['ifsccode'] = $row['ifsccode'];
                $res['branchname'] = $row['branchname'];
                $res['modifieddate'] = $row['modifieddate'];
                array_push($a, $res);
            }
        }
        echo json_encode($a);
    }
    // ///////////////// end of member ///////////////////////////

    ////////////////////// login api ////////////////////////////
    public function userLogin($username, $password)
    {
        $q = "SELECT * FROM `User` WHERE `username` = '$username' AND `password` = '$password'";
        $query = mysqli_query($this->conn, $q);
        // $a = array();
        $res = array();
        if ($query->num_rows > 0) {
            // output data of each row
            while ($row = $query->fetch_assoc()) {
                $res['status'] = "login_successful";
                $res['code'] = '200';
                $res['user_id'] = $row["user_id"];
                $res['lastname'] = $row["lastname"];
                $res['phonenumber'] = $row['phonenumber'];
                $res['address'] = $row['address'];
                $res['role'] = $row['role'];
                $res['joindate'] = $row['joindate'];
                $res['dob'] = $row['dob'];
            }
        } else {
            $res['status'] = "login_failed";
            $res['code'] = '400';
        }
        echo json_encode($res);
    }

    ////////////////////// end of login /////////////////////////

    ///////// count ///////
    public function getCount()
    {
        $res = array();
        $res['group_count'] = $this->getGroupCount();
        $res['director_count'] = $this->getUserAsDirectorCount();
        $res['user_count'] = $this->getUserAsAgentCount();
        $res['member_count'] = $this->getMemberCount();
        echo json_encode($res);

    }

    public function getGroupCount()
    {
        $q = "SELECT COUNT(*) as 'group_count' FROM `groupMaster`";
        $query = mysqli_query($this->conn, $q);
        $res = 0;
        if ($query->num_rows > 0) {
            // output data of each row
            while ($row = $query->fetch_assoc()) {
                $res = $row['group_count'];
            }
        }
        return $res;
    }

    public function getUserAsDirectorCount()
    {
        $q = "SELECT COUNT(*) as 'director_count' FROM `User` WHERE `role` = 'Director'";
        $query = mysqli_query($this->conn, $q);
        $res = 0;
        if ($query->num_rows > 0) {
            // output data of each row
            while ($row = $query->fetch_assoc()) {
                $res = $row['director_count'];
            }
        }
        return $res;
    }

    public function getUserAsAgentCount()
    {
        $q = "SELECT COUNT(*) as 'user_count' FROM `User` WHERE `role` = 'agent'";
        $query = mysqli_query($this->conn, $q);
        $res = 0;
        if ($query->num_rows > 0) {
            // output data of each row
            while ($row = $query->fetch_assoc()) {
                $res = $row['user_count'];
            }
        }
        return $res;
    }

    public function getMemberCount()
    {
        $q = "SELECT COUNT(*) as 'member_count' FROM `member`";
        $query = mysqli_query($this->conn, $q);
        $res = 0;
        if ($query->num_rows > 0) {
            // output data of each row
            while ($row = $query->fetch_assoc()) {
                $res = $row['member_count'];
            }
        }
        return $res;
    }

    public function getRole()
    {
        $q = "SELECT * FROM `role`";
        $query = mysqli_query($this->conn, $q);
        $a = array();
        $res = array();
        if ($query->num_rows > 0) {
            // output data of each row
            while ($row = $query->fetch_assoc()) {
                $res['role_id'] = $row["role_id"];
                $res['role_name'] = $row["role_name"];
                array_push($a, $res);
            }
        }
        echo json_encode($a);
    }
    public function getAttadence()
    {
        $q = "SELECT * FROM `attadence_type`";
        $query = mysqli_query($this->conn, $q);
        $a = array();
        $res = array();
        if ($query->num_rows > 0) {
            // output data of each row
            while ($row = $query->fetch_assoc()) {
                $res['id'] = $row["id"];
                $res['attadence_type'] = $row["attadence_type"];
                array_push($a, $res);
            }
        }
        echo json_encode($a);
    }

    public function getUserName($user_id)
    {
        $user_id = intval($user_id);
        $q = "SELECT `firstname` FROM `User` where `user_id` =`$user_id`";
        $query = mysqli_query($this->conn, $q);
        if ($query->num_rows > 0) {
            // output data of each row
            while ($row = $query->fetch_assoc()) {
                return $row["firstname"];
            }
        }
    }

    /////////////////////////////CREATE LEDGER //////////////////////////

    public function createLedger($ledger_reg_id, $name, $address, $city, $phone1, $phone2, $email, $openbalance, $closebalance, $user_id, $group_id, $group_name, $typeofledger, $drcr, $type_id, $approvedby)
    {
        $modifieddate = $this->getCurrentDate();
        $query_set = "INSERT INTO `Ledger`(`ledger_reg_id`, `name`, `address`, `city`, `phone1`, `phone2`, `email`, `openbalance`, `closebalance`, `createdby`, `approvedby`, `user_id`, `modifieddate`, `group_id`, `group_name`, `typeofledger`, `drcr`, `type_id`) VALUES
        ('$ledger_reg_id','$name','$address','$city','$phone1','$phone2','$email','$openbalance','$closebalance','$modifieddate','$approvedby','$user_id','$modifieddate','$group_id','$group_name','$typeofledger','$drcr','$type_id')";
        $query = mysqli_query($this->conn, $query_set);
        if ($query) {
            return 0;
        } else {
            return 1;
        }
    }

    public function editLedger($ledger_id, $ledger_reg_id, $name, $address, $city, $phone1, $phone2, $email, $openbalance, $closebalance, $user_id, $group_id, $group_name, $typeofledger, $drcr, $type_id, $approvedby)
    {
        $modified_date = $this->getCurrentDate();
        $response = array();
        $query_set = "UPDATE `Ledger` SET `ledger_reg_id`='$ledger_reg_id',`name`='$name',`address`='$address',`city`='$city',`phone1`='$phone1',`phone2`='$phone2',`email`='$email',`openbalance`='$openbalance',`closebalance`='$closebalance',`user_id`='$user_id',`modifieddate`='$modified_date',`group_id`='$group_id',`group_name`='$group_name',`typeofledger`='$typeofledger',`drcr`='$drcr',`type_id`='$type_id', `approvedby`='$approvedby' WHERE `ledger_id`='$ledger_id'";
        $query = mysqli_query($this->conn, $query_set);
        if ($query) {
            $obj = (object) [
                'status' => "successful",
                'status_code' => '1',
            ];
            echo json_encode($obj);
        } else {
            $obj = (object) [
                'status' => "failed",
                'status' => '0',
            ];
            echo json_encode($obj);
        }
    }

    public function deleteLedger($ledger_id)
    {
        $response = array();
        $query_set = "DELETE FROM `Ledger` WHERE `ledger_id`='$ledger_id'";
        $query = mysqli_query($this->conn, $query_set);
        if ($query) {
            return 0;
        } else {
            return 1;
        }
    }

    public function getLedger()
    {
        $q = "SELECT * FROM `Ledger`";
        $query = mysqli_query($this->conn, $q);
        $a = array();
        $res = array();
        if ($query->num_rows > 0) {
            // output data of each row
            while ($row = $query->fetch_assoc()) {
                $res['ledger_id'] = $row["ledger_id"];
                $res['ledger_reg_id'] = $row["ledger_reg_id"];
                $res['name'] = $row["name"];
                $res['address'] = $row["address"];
                $res['city'] = $row["city"];
                $res['phone1'] = $row["phone1"];
                $res['phone2'] = $row["phone2"];
                $res['email'] = $row["email"];
                $res['openbalance'] = $row["openbalance"];
                $res['closebalance'] = $row["closebalance"];
                $res['createdby'] = $row["createdby"];
                $res['approvedby'] = $row["approvedby"];
                $res['user_id'] = $row["user_id"];
                $res['modifieddate'] = $row["modifieddate"];
                $res['group_id'] = $row["group_id"];
                $res['group_name'] = $row["group_name"];
                $res['typeofledger'] = $row["typeofledger"];
                $res['drcr'] = $row["drcr"];
                $res['type_id'] = $row["type_id"];
                array_push($a, $res);
            }
        }
        echo json_encode($a);
    }

    public function getLedgerLastID()
    {
        $q = "SELECT `ledger_id` FROM `Ledger` ORDER BY `ledger_id` DESC LIMIT 1";
        $query = mysqli_query($this->conn, $q);
        $a = array();
        $res = array();
        if ($query->num_rows > 0) {
            // output data of each row
            while ($row = $query->fetch_assoc()) {
                $res['ledger_id'] = $row["ledger_id"];
                // array_push($a, $res);
            }
        } else {
            $res['ledger_id'] = 0;
        }
        echo json_encode($res);

    }

    public function createTypeLedger($type_ledger, $parent_id)
    {
        $modifieddate = $this->getCurrentDate();
        $query_set = "INSERT INTO `typeofledger`(`type_name`,`parent_id`, `modified_date`) VALUES ('$type_ledger','$parent_id','$modifieddate')";
        $query = mysqli_query($this->conn, $query_set);
        if ($query) {
            return 0;
        } else {
            return 1;
        }
    }

    public function getLedgerType()
    {
        $q = "SELECT * FROM `typeofledger`";
        $query = mysqli_query($this->conn, $q);
        $a = array();
        $res = array();
        if ($query->num_rows > 0) {
            // output data of each row
            while ($row = $query->fetch_assoc()) {
                $res['type_id'] = $row["type_id"];
                $res['type_name'] = $row["type_name"];
                $res['parent_id'] = $row["parent_id"];
                $res['modified_date'] = $row["modified_date"];
                array_push($a, $res);
            }
        }
        echo json_encode($a);
    }

    public function editLedgerType($type_id, $type_ledger, $parent_id)
    {
        $modifieddate = $this->getCurrentDate();
        $response = array();
        $query_set = "UPDATE `typeofledger` SET `type_name`='$type_ledger',`parent_id`='$parent_id',`modified_date`='$modifieddate' WHERE `type_id` ='$type_id'";
        $query = mysqli_query($this->conn, $query_set);
        if ($query) {
            $obj = (object) [
                'status' => "successful",
                'status_code' => '1',
            ];
            echo json_encode($obj);
        } else {
            $obj = (object) [
                'status' => "failed",
                'status' => '0',
            ];
            echo json_encode($obj);

        }
    }

    public function deleteLedgerType($type_id)
    {
        $response = array();
        $query_set = "DELETE FROM `typeofledger` WHERE `type_id`='$type_id'";
        $query = mysqli_query($this->conn, $query_set);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }

    ////////////////////////////LEDGER END ////////////////////////////////

    public function createAttadence($user_id, $name, $status, $date)
    {
        $checkData = mysqli_query($this->conn, "SELECT * FROM `Attadence_user` WHERE `user_id`='$user_id' AND `date`='$date'");
        if (mysqli_num_rows($checkData) > 0) {
            $obj = (object) [
                'status' => "Already Added for this Date for this user",
                'status_code' => '200',
            ];
            echo json_encode($obj);
        } else {
            $modifieddate = $this->getCurrentDate();
            $query_set = "INSERT INTO `Attadence_user`(`user_id`, `name`, `date`, `status`, `modifiedDate`) VALUES ('$user_id','$name','$date','$status','$modifieddate')";
            $query = mysqli_query($this->conn, $query_set);
            if ($query) {
                $obj = (object) [
                    'status' => "Successfully_added",
                    'status_code' => '200',
                ];
                echo json_encode($obj);
            } else {
                $obj = (object) [
                    'status' => "Failed_add",
                    'status_code' => '400',
                ];
                echo json_encode($obj);
            }
        }
    }
    //////////////////Voucher type /////////////
    public function createVoucherType($voucher_type)
    {
        $modifieddate = $this->getCurrentDate();
        $query_set = "INSERT INTO `VoucherMaster`(`voucher_name`,`modified_date`) VALUES ('$voucher_type','$modifieddate')";
        $query = mysqli_query($this->conn, $query_set);
        if ($query) {
            return 0;
        } else {
            return 1;
        }
    }

    public function getVoucherType()
    {
        $q = "SELECT * FROM `VoucherMaster`";
        $query = mysqli_query($this->conn, $q);
        $a = array();
        $res = array();
        if ($query->num_rows > 0) {
            // output data of each row
            while ($row = $query->fetch_assoc()) {
                $res['voucher_id'] = $row["voucher_id"];
                $res['voucher_name'] = $row["voucher_name"];
                $res['modified_date'] = $row["modified_date"];
                array_push($a, $res);
            }
        }
        echo json_encode($a);
    }

    public function editVoucherType($voucher_id, $voucher_type)
    {
        $modifieddate = $this->getCurrentDate();
        $response = array();
        $query_set = "UPDATE `VoucherMaster` SET `voucher_name`='$voucher_type',`modified_date`='$modifieddate' WHERE `voucher_id` ='$voucher_id'";
        $query = mysqli_query($this->conn, $query_set);
        if ($query) {
            $obj = (object) [
                'status' => "successful",
                'status_code' => '1',
            ];
            echo json_encode($obj);
        } else {
            $obj = (object) [
                'status' => "failed",
                'status' => '0',
            ];
            echo json_encode($obj);

        }
    }

    public function deleteVoucherType($voucher_id)
    {
        $response = array();
        $query_set = "DELETE FROM `VoucherMaster` WHERE `voucher_id`='$voucher_id'";
        $query = mysqli_query($this->conn, $query_set);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }

    ///////////////// voucher type end //////////////////////////

    ///////////////////// cashbook tranasaction //////////////////

    public function getCashBook()
    {
        $q = "SELECT * FROM `cashBank`";
        $query = mysqli_query($this->conn, $q);
        $a = array();
        $res = array();
        if ($query->num_rows > 0) {
            // output data of each row
            while ($row = $query->fetch_assoc()) {
                $res['voucher_id'] = $row["voucher_id"];
                $res['voucher_name'] = $row["voucher_name"];
                $res['voucher_number'] = $row["voucher_number"];
                $res['source_id'] = $row["source_id"];
                $res['source_name'] = $row["source_name"];
                $res['account_id'] = $row["account_id"];
                $res['account_name'] = $row["account_name"];
                $res['fyear'] = $row["fyear"];
                $res['voucher_date'] = $row["voucher_date"];
                $res['narration'] = $row["narration"];
                $res['amount'] = $row["amount"];
                $res['createdBy'] = $row["createdBy"];
                $res['modifiedDate'] = $row["modifiedDate"];
                array_push($a, $res);
            }
        }
        echo json_encode($a);
    }

    public function checkVoucherExists($voucher_number, $voucher_name)
    {
        $result = false;
        $q = "SELECT * FROM `cashBank` WHERE `voucher_number`= '$voucher_number' AND `voucher_name` LIKE ('$voucher_name')";
        $query = mysqli_query($this->conn, $q);
        if (mysqli_num_rows($query) >= 1) {
            $result = true;
        }
        return $result;
    }

    public function getLedgerById($ledger_id, $callFrom) {
        $q = "SELECT * FROM `Ledger` WHERE `ledger_id` = '$ledger_id'";
        $query = mysqli_query($this->conn, $q);
        if (mysqli_num_rows($query) >= 1) {
            while ($row = $query->fetch_assoc()) {
                $res['ledger_id'] = $row["ledger_id"];
                $res['ledger_reg_id'] = $row["ledger_reg_id"];
                $res['name'] = $row["name"];
                $res['address'] = $row["address"];
                $res['city'] = $row["city"];
                $res['phone1'] = $row["phone1"];
                $res['phone2'] = $row["phone2"];
                $res['email'] = $row["email"];
                $res['openbalance'] = $row["openbalance"];
                $res['closebalance'] = $row["closebalance"];
                $res['createdby'] = $row["createdby"];
                $res['approvedby'] = $row["approvedby"];
                $res['user_id'] = $row["user_id"];
                $res['modifieddate'] = $row["modifieddate"];
                $res['group_id'] = $row["group_id"];
                $res['group_name'] = $row["group_name"];
                $res['typeofledger'] = $row["typeofledger"];
                $res['drcr'] = $row["drcr"];
                $res['type_id'] = $row["type_id"];
            }

        }
        if ($callFrom == "web") {
            echo json_encode($a);
        } else {
            return json_encode($a);
        }
        
    }

    public function getLedgerByIdName($ledger_id) {
        $q = "SELECT * FROM `Ledger` WHERE `ledger_id` = '$ledger_id'";
        $query = mysqli_query($this->conn, $q);
        if (mysqli_num_rows($query) >= 1) {
            while ($row = $query->fetch_assoc()) {
                $res = $row["name"];
            }
        }
        return $res;   
    }

    public function addCashBank($obj)
    {
        $modifieddate = $this->getCurrentDate(); 
        ////////////////////////////////////////
        $voucher_number = $obj['voucher_number'];
        $voucher_name = $obj['voucher_name'];
        $isVoucherExist = $this->checkVoucherExists($voucher_number,$voucher_name);
        if ($isVoucherExist == true) {
            $object = (object) [
                'status' => "Voucher Exist",
                'message' => "Voucher allready Exists",
                'status_code' => '201',
            ];
        } else {
            $account_list = $obj['accountList'];
            $res = false;
            foreach ($account_list as $accounts) {
                $sourceid = $obj['sourceid'];
                $sourcename = $this->getLedgerByIdName($sourceid);
                $account_id = $accounts['account_id'];
                $accountname = $this->getLedgerByIdName($account_id);
                $narration = $obj['narration'];
                $fyear = $this->getYear();
                $voucher_date = $obj['voucher_date'];
                $amount = $accounts['amount'];
                $user_id = $obj['user_id'];
                $query_set = "INSERT INTO `cashBank`(`voucher_name`, `voucher_number`, `source_id`, `source_name`, `account_id`, `account_name`, `fyear`, `voucher_date`, `narration`, `amount`, `createdBy`, `modifiedDate`) VALUES
                ('$voucher_name','$voucher_number','$sourceid','$sourcename','$account_id','$accountname',$fyear,$voucher_date,'$narration','$amount','$user_id','$modifieddate')";
                $query = mysqli_query($this->conn, $query_set);
                echo "$query";
                if ($query) {
                    $res = true;
                } else {
                    $res = false;
                }
            }
            if ($res == true) {
                $object = (object) [
                    'status' => "Cash_Book_Added",
                    'message' => "CashBook Added SuccessFully",
                    'status_code' => '200',
                ];
            } else {
                $object = (object) [
                    'status' => "Cash_Book_failed",
                    'message' => "CashBook insertion failed",
                    'status_code' => '202',
                ];
            } 
        }
        
    }

}
