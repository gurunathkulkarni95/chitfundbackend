<?php
/**
 * Project Name : ChitFund Software
 * Frontend developer : Pooja Nandnikar
 * @author Gurunath kulkarni
 */
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header("Access-Control-Allow-Headers: X-Requested-With");
require_once '../include/DbHandler.php';
require_once '../include/DbConnect.php';
header('Content-Type: application/json');
$json = file_get_contents('php://input');
$method = $_SERVER['REQUEST_METHOD'];
$db = new DbHandler();
if ($method == "POST") {
    $obj = json_decode($json, true);
    $ledger_reg_id = $obj['ledger_reg_id'];
    $name = $obj['name'];
    $address = $obj['address'];
    $city = $obj['city'];
    $phone1 = $obj['phone1'];
    $phone2 = $obj['phone2'];
    $email = $obj['email'];
    $openbalance = $obj['openbalance'];
    $closebalance = $obj['closebalance'];
    $approvedby = $obj['approvedby'];
    $user_id = $obj['user_id'];
    $group_id = $obj['group_id'];
    $group_name = $obj['group_name'];
    $typeofledger = $obj['typeofledger'];
    $drcr = $obj['drcr'];
    $type_id = $obj['type_id'];

    $res = $db->createLedger($ledger_reg_id, $name, $address, $city, $phone1, $phone2, $email, $openbalance, $closebalance, $user_id, $group_id, $group_name, $typeofledger, $drcr, $type_id,$approvedby);
    if ($res == 0) {
        $response["status"] = "Successfull";
        $response["code"] = "200";
        echo json_encode($response);
    } else {
        $response["status"] = "failed";
        $response["code"] = "400";
        echo json_encode($response);

    }
} else if ($method == "GET") {
    $option = $_GET["option"];
    if ($option == "all") {
        $res = $db->getLedger();
    } else if ($option == "id") {
        $res = $db->getLedgerLastID();
    }

} else if ($method == "PUT") {
    $obj = json_decode($json, true);
    $ledger_id=$obj['ledger_id'];
    $ledger_reg_id = $obj['ledger_reg_id'];
    $name = $obj['name'];
    $address = $obj['address'];
    $city = $obj['city'];
    $phone1 = $obj['phone1'];
    $phone2 = $obj['phone2'];
    $email = $obj['email'];
    $openbalance = $obj['openbalance'];
    $closebalance = $obj['closebalance'];
    $approvedby = $obj['approvedby'];
    $user_id = $obj['user_id'];
    $group_id = $obj['group_id'];
    $group_name = $obj['group_name'];
    $typeofledger = $obj['typeofledger'];
    $drcr = $obj['drcr'];
    $type_id = $obj['type_id'];

    $res = $db->editLedger($ledger_id,$ledger_reg_id, $name, $address, $city, $phone1, $phone2, $email, $openbalance, $closebalance, $user_id, $group_id, $group_name, $typeofledger, $drcr, $type_id);
} else if ($method == "DELETE") {
    $obj = json_decode($json, true);
    $ledger_id=$obj['ledger_id'];
    $res = $db->deleteLedger($ledger_id);
    if ($res == 0) {
        $response["status"] = "Successfull";
        $response["code"] = "200";
        echo json_encode($response);
    } else {
        $response["status"] = "failed";
        $response["code"] = "400";
        echo json_encode($response);

    }
    
}
