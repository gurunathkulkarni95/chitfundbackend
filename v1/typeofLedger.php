<?php
/**
 * Project Name : ChitFund Software
 * Frontend developer : Pooja Nandnikar
 * @author Gurunath kulkarni
 */
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header("Access-Control-Allow-Headers: X-Requested-With");
require_once '../include/DbHandler.php';
require_once '../include/DbConnect.php';
header('Content-Type: application/json');
$json = file_get_contents('php://input');
$method = $_SERVER['REQUEST_METHOD'];
$db = new DbHandler();
if ($method == "POST") {
    $obj = json_decode($json, true);
    $type_ledger = $obj['type_ledger'];
    $parent_id = $obj['parent_id'];

    $res = $db->createTypeLedger($type_ledger,$parent_id);
    if ($res == 0) {
        $response["status"] = "success";
        $response["code"] = "200";
        echo json_encode($response);
    } else {
        $response["status"] = "failed";
        $response["code"] = "400";
        echo json_encode($response);

    }
} else if ($method == "GET") {
    $res = $db->getLedgerType();
} else if ($method == "PUT") {
    $obj = json_decode($json, true);
    $type_id=$obj['type_id'];
    $type_ledger = $obj['type_ledger'];
    $parent_id = $obj['parent_id'];

    $res = $db->editLedgerType($type_id,$type_ledger,$parent_id);
} else if ($method == "DELETE") {
    $obj = json_decode($json, true);
    $type_id=$obj['type_id'];
    $res = $db->deleteLedgerType($type_id);
    if ($res == 0) {
        $response["status"] = "failed";
        $response["code"] = "400";
        echo json_encode($response);
    } else {
        $response["status"] = "Successfull";
        $response["code"] = "200";
        echo json_encode($response);

    }
}
