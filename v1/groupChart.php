<?php
/**
 * Project Name : ChitFund Software
 * Frontend developer : Pooja Nandnikar
 * @author Gurunath kulkarni
 */
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header("Access-Control-Allow-Headers: X-Requested-With");
require_once '../include/DbHandler.php';
require_once '../include/DbConnect.php';
header('Content-Type: application/json');
$json = file_get_contents('php://input');
$method = $_SERVER['REQUEST_METHOD'];
$db = new DbHandler();
if ($method == "POST") {
    $obj = json_decode($json, true);
    foreach ($obj as $value) {
        $group_id = $value['group_id'];
        $fixbidding_per = $value['fixbidding_per'];
        $dividend = $value['dividend'];
        $sub = $value['sub'];
        $prize_amt = $value['prize_amt'];
        $res = $db->createGroupChart($group_id,$fixbidding_per, $dividend, $sub, $prize_amt);
    }
    if ($res == 0) {
        $response["status"] = "Successfull";
        $response["code"] = "200";
        echo json_encode($response);

    } else {
        $response["status"] = "failed";
        $response["code"] = "400";
        echo json_encode($response);

    }
}
// else if ($method == "GET") {
//     $res= $db->getDirector();
// } else if ($method == "PUT") {
//     $obj = json_decode($json, true);
//     $director_id = $obj['director_id'];
//     $director_name = $obj['director_name'];
//     $director_address = $obj['director_address'];
//     $phonenumber = $obj['phonenumber'];
//     $res = $db->EditDirector($director_id,$director_name, $director_address, $phonenumber);
//     if ($res == 0) {
//         $response["status"] = "failed";
//         $response["code"] = "400";
//         echo json_encode($response);
//     } else {
//         $response["status"] = "Successfull";
//         $response["code"] = "200";
//         echo json_encode($response);

//     }
// } else if ($method == "DELETE") {
//     $obj = json_decode($json, true);
//     $director_id = $obj['director_id'];
//     $res = $db->directorDelete($director_id);
//     if ($res == 0) {
//         $response["status"] = "failed";
//         $response["code"] = "400";
//         echo json_encode($response);
//     } else {
//         $response["status"] = "Successfull";
//         $response["code"] = "200";
//         echo json_encode($response);

//    }
// }
