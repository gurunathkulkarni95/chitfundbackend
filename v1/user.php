<?php
/**
 * Project Name : ChitFund Software
 * Frontend developer : Pooja Nandnikar
 * @author Gurunath kulkarni
 */
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header("Access-Control-Allow-Headers: X-Requested-With");
require_once '../include/DbHandler.php';
require_once '../include/DbConnect.php';
header('Content-Type: application/json');
$json = file_get_contents('php://input');
$method = $_SERVER['REQUEST_METHOD'];
$db = new DbHandler();
if ($method == "POST") {
    $obj = json_decode($json, true);
    $user_fname = $obj['user_fname'];
    $user_lname = $obj['user_lname'];
    $phonenumber = $obj['phonenumber'];
    $user_address = $obj['user_address'];
    $username = $obj['username'];
    $password = $obj['password'];
    $join_date = $obj['join_date'];
    $modified_date = $obj['modified_date'];
    $dob = $obj['dob'];
    $role = $obj['role'];
    $modified_date = $obj['modified_date'];

    $res = $db->createUser($user_fname, $user_lname, $phonenumber,$user_address,$username,$password,$join_date,$dob,$role,$modified_date);
    if ($res == 0) {
        $response["status"] = "success";
        $response["code"] = "200";
        echo json_encode($response);
    } else {
        $response["status"] = "failed";
        $response["code"] = "400";
        echo json_encode($response);

    }
} else if ($method == "GET") {
    $res = $db->getUser();
} else if ($method == "PUT") {
    $obj = json_decode($json, true);
    $user_id = $obj['user_id'];
    $user_fname = $obj['user_fname'];
    $user_lname = $obj['user_lname'];
    $phonenumber = $obj['phonenumber'];
    $user_address = $obj['user_address'];
    $username = $obj['username'];
    $password = $obj['password'];
    $join_date = $obj['join_date'];
    $modified_date = $obj['modified_date'];
    $dob = $obj['dob'];
    $role = $obj['role'];

    $res = $db->EditUser($user_id,$user_fname, $user_lname, $phonenumber,$user_address,$username,$password,$join_date,$dob,$role,$modified_date);
    // if ($res == 0) {
    //     $response["status"] = "success";
    //     $response["code"] = "200";
    //     echo json_encode($response);
    // } else {
    //     $response["status"] = "failed";
    //     $response["code"] = "400";
    //     echo json_encode($response);

    // }
} else if ($method == "DELETE") {
    $obj = json_decode($json, true);
    $user_id = $obj['user_id'];
    $res = $db->DeleteUser($user_id);
    if ($res == 0) {
        $response["status"] = "failed";
        $response["code"] = "400";
        echo json_encode($response);
    } else {
        $response["status"] = "Successfull";
        $response["code"] = "200";
        echo json_encode($response);

    }
} else if($method == "OPTIONS") {
    $obj = json_decode($json, true);
    $option = $obj['option'];
    $res = $db->getUserAsOption($option);
}


?>
