<?php
/**
 * Project Name : ChitFund Software
 * Frontend developer : Pooja Nandnikar
 * @author Gurunath kulkarni
 */
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
require_once '../include/DbHandler.php';
require_once '../include/DbConnect.php';
header('Content-Type: application/json');
$json = file_get_contents('php://input');
$method = $_SERVER['REQUEST_METHOD'];
$db = new DbHandler();
if ($method == "POST") {
    $obj = json_decode($json, true);
    $group_name = $obj['group_name'];
    $member_count = $obj['member_count'];
    $no_of_months = $obj['no_of_months'];
    $amount_value = $obj['amount_value'];
    $installement_amount = $obj['installement_amount'];
    $start_Date = $obj['start_date'];
    $end_date = $obj['end_date'];
    $res = $db->createGroup($group_name, $member_count, $no_of_months, $amount_value, $installement_amount,$start_Date,$end_date);
} else if ($method == "GET") {
    $res = $db->getGroup();
} else if ($method == "PUT") {
    $obj = json_decode($json, true);
    $group_id = $obj['group_id'];
    $group_name = $obj['group_name'];
    $member_count = $obj['member_count'];
    $no_of_months = $obj['no_of_months'];
    $amount_value = $obj['amount_value'];
    $installement_amount = $obj['installement_amount'];
    $start_Date = $obj['start_date'];
    $end_date = $obj['end_date'];
    $res = $db->EditGroup($group_id, $group_name, $member_count, $no_of_months, $amount_value, $installement_amount,$start_Date,$end_date);
} else if ($method == "DELETE") {
    $obj = json_decode($json, true);
    $director_id = $obj['group_id'];
    $res = $db->GroupDelete($group_id);
    if ($res == 0) {
        $response["status"] = "failed";
        $response["code"] = "400";
        echo json_encode($response);
    } else {
        $response["status"] = "Successfull";
        $response["code"] = "200";
        echo json_encode($response);

    }
}
