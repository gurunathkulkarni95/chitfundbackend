<?php
/**
 * Project Name : ChitFund Software
 * Frontend developer : Pooja Nandnikar
 * @author Gurunath kulkarni
 */
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header("Access-Control-Allow-Headers: X-Requested-With");
require_once '../include/DbHandler.php';
require_once '../include/DbConnect.php';
header('Content-Type: application/json');
$json = file_get_contents('php://input');
$method = $_SERVER['REQUEST_METHOD'];
$db = new DbHandler();
if ($method == "POST") {
    $obj = json_decode($json, true);
    $bank_name = $obj['bank_name'];
    $account_number = $obj['account_number'];
    $ifsccode = $obj['ifsccode'];
    $branchname = $obj['branchname'];
    $modified_date = $obj['modified_date'];

    $res = $db->createBank($bank_name,$account_number,$ifsccode, $branchname,$modified_date);
    if ($res == 0) {
        $response["status"] = "success";
        $response["code"] = "200";
        echo json_encode($response);
    } else {
        $response["status"] = "failed";
        $response["code"] = "400";
        echo json_encode($response);

    }
} else if ($method == "GET") {
    $res = $db->getBank();
} else if ($method == "PUT") {
    $obj = json_decode($json, true);
    $bank_id=$obj['bank_id'];
    $bank_name = $obj['bank_name'];
    $account_number = $obj['account_number'];
    $ifsccode = $obj['ifsccode'];
    $branchname = $obj['branchname'];
    $modified_date = $obj['modified_date'];

    $res = $db->EditBank($bank_id,$bank_name,$account_number,$ifsccode, $branchname,$modified_date);
} else if ($method == "DELETE") {
    $obj = json_decode($json, true);
    $bank_id = $obj['bank_id'];
    $res = $db->deleteBank($bank_id);
    if ($res == 0) {
        $response["status"] = "failed";
        $response["code"] = "400";
        echo json_encode($response);
    } else {
        $response["status"] = "Successfull";
        $response["code"] = "200";
        echo json_encode($response);

    }
}
