<?php
/**
 * Project Name : ChitFund Software
 * Frontend developer : Pooja Nandnikar
 * @author Gurunath kulkarni
 */
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
require_once '../include/DbHandler.php';
require_once '../include/DbConnect.php';
header('Content-Type: application/json');
$json = file_get_contents('php://input');
$method = $_SERVER['REQUEST_METHOD'];
$db = new DbHandler();
if ($method == "POST") {
    $obj = json_decode($json, true);
    $organization_name = $obj['organization_name'];
    $organization_address = $obj['organization_address'];
    $organization_date = $obj['organization_date'];
    $phone_number = $obj['phone_number'];
    $res = $db->createOrganization($organization_name, $organization_address, $organization_date, $phone_number);
    if ($res == 0) {
        $response["status"] = "failed";
        $response["code"] = "400";
        echo json_encode($response);
    } else {
        $response["status"] = "Successfull";
        $response["code"] = "200";
        echo json_encode($response);

    }
} else if ($method == "GET") {
    $res = $db->getDirector();
} else if ($method == "PUT") {
    $obj = json_decode($json, true);
    $organization_id = $obj['organization_id'];
    $organization_name = $obj['organization_name'];
    $organization_address = $obj['organization_address'];
    $organization_date = $obj['organization_date'];
    $phone_number = $obj['phone_number'];
    $res = $db->EditOrganization($organization_id, $organization_name, $organization_address, $organization_date, $phone_number);
    if ($res == 0) {
        $response["status"] = "failed";
        $response["code"] = "400";
        echo json_encode($response);
    } else {
        $response["status"] = "Successfull";
        $response["code"] = "200";
        echo json_encode($response);

    }
} else if ($method == "DELETE") {
    echo "delete";
}
